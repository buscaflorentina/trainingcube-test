import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScheduledExercises extends StatelessWidget {

  final int exerciseNo;
  final int repetitionsNo;
  final String exerciseName;
  final int exerciseDuration;


  ScheduledExercises(this.exerciseNo,this.repetitionsNo,this.exerciseName,this.exerciseDuration);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
        Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child:Image.asset('images/girl$exerciseNo.png',
                    height: 50.0,
                    width: 50.0,
                    fit:BoxFit.fill),
                ),),



                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Exercises '+exerciseNo.toString(),style: TextStyle(
                      color: Color(0xFF303030),
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                    ), ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: <Widget>[
                        Text(repetitionsNo.toString()+'x',style:TextStyle(
                          color:Color(0xFF707070),
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500,
                        ),),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(exerciseName,style: TextStyle(
                          color:Color(0xFF393E46),
                          fontSize: 14.0,
                          fontWeight: FontWeight.w500,
                        ),)
                      ],
                    ),

                  ],
                ),

                Text(exerciseDuration.toString()+ 'mins',
                  style:TextStyle(
                    color:Color(0xFFE9707D),
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                  ),),







    ],

            ),
            SizedBox(height: 10.0,),
          ],
        ),);
  }
}
