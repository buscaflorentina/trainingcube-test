import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WorkoutDetails extends StatelessWidget {

  final int exercisesNo;
  final int participantsNo;
  final int maxParticipantsNo;
  final int duration;


  WorkoutDetails(this.exercisesNo,this.participantsNo,this.maxParticipantsNo,this.duration);

  @override
  Widget build(BuildContext context) {

    TextStyle textStyle1=TextStyle(
                    color:Color(0xFF707070),
                    fontSize: 13.0,
                      fontWeight: FontWeight.w500,
                  );
    TextStyle textStyle2=TextStyle(
      color:Color(0xFF303030),
      fontSize: 17.0,
      fontWeight: FontWeight.w500,
    );

    return Container(
      child:Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
            Column(
                children: <Widget>[
                Text('Exercises',style: textStyle1,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(children: <Widget>[
                     DetailsElement(Color(0xFFFF638A),Icons.fitness_center,),
                     SizedBox(
                       width:5.0,
                     ),
                     Text(exercisesNo.toString(),style: textStyle2,),
      ],)

        ],
      ),
             Column(
                children: <Widget>[
                  Text('Participants',style: textStyle1,),
                  SizedBox(
                    height: 5.0,
                  ),
                   Row(children: <Widget>[
                         DetailsElement(Color(0xFFC08BEE),Icons.sort,),
                         SizedBox(
                           width:5.0,
                         ),
                         Text(participantsNo.toString()+'/'+maxParticipantsNo.toString(),style: textStyle2,),
    ],),
    ],),
          Column(
            children: <Widget>[
              Text('Duration',style:textStyle1),
              SizedBox(
                height: 5.0,
              ),
              Row(children: <Widget>[
                DetailsElement(Color(0xFFFFA81A),Icons.access_time,),
                SizedBox(
                  width:5.0,
                ),
                Text(duration.toString()+' min',style: textStyle2,),
              ],)
            ],)
          ,],),);
  }
}

class DetailsElement extends StatelessWidget {

  final Color colour;
  final IconData icon;

  DetailsElement(this.colour,this.icon);



  @override
  Widget build(BuildContext context) {
    return Container(
      child:Column(
        children: <Widget>[
          Row(

            children: <Widget>[
              Container(
              padding: EdgeInsets.only(left:1.0, right: 1.0, top: 1.0, bottom: 1.0),
              decoration: BoxDecoration(
                color: colour,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(5.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.5),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Center(
                child:Icon(icon,color: Colors.white,),),
            ),

            ],
          ),
        ],
      ),
    );
  }
}

