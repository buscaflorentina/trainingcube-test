import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class PictureStyle extends StatelessWidget {
  final double radius1;
  final double radius2;

  final int cardNumber;


  PictureStyle(this.radius1,this.radius2,this.cardNumber);

  @override
  Widget build(BuildContext context) {
    return Card(
        shadowColor: Color.fromARGB(25, 0, 0, 0),
        color: Colors.grey[100],
        elevation: 20.0,
        shape: CircleBorder(),
        child: Stack(
          alignment: Alignment.center,
            children: <Widget>[
              CircleAvatar(
                radius:radius1,
                backgroundColor: Colors.white),
              CircleAvatar(
                backgroundImage: AssetImage('images/girl$cardNumber.png'),
                radius:radius2,


    ),
    ],),);






  }
}




