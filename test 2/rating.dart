import 'package:flutter/material.dart';

class Rating extends StatelessWidget {

  final int rating;
  Rating(this.rating);

  List<Widget> WorkoutRating=[
    Star(Icons.star),
    Star(Icons.star),
    Star(Icons.star),
    Star(Icons.star),
    Star(Icons.star_border),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child:Row(
        children: WorkoutRating,

      ),
    );
  }
}

class Star extends StatelessWidget {

  IconData starType;
  Star(this.starType);

  @override
  Widget build(BuildContext context) {
    return Icon(starType,color: Color(0xFFF7B500),
    size: 14.0,);
  }
}
