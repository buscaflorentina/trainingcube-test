import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:texttrainingcube2/picture_style.dart';
import 'screensize_reducers.dart';
import 'rating.dart';
import 'workout_details.dart';
import 'scheduled_exercises.dart';


void main() {
  runApp(MyApp());
}

List <Widget> exercisesList=[
  ScheduledExercises(1,20,'Leg Raises',30),
  ScheduledExercises(2,30,'Leg Raises2',20),
  ScheduledExercises(3,50,'Leg Raises3',40)

];

class MyApp extends StatelessWidget {
  final int reviewNo=27;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xFFF2F0F0),
        body: Column(
        children: <Widget>[
          Container(
              //width: screenWidth(context, multiplyFraction: 0.1),
              //height: screenHeight(context, multiplyFraction: 0.1),
            margin: EdgeInsets.only(top:60.0),
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    WorkoutName('PILATES'),
                    WorkoutDate('4 April','10:00 AM'),
                    TrainerName('Jerome'),
                    Container(
                      width:150.0,
                      child: Stack(
                        children: <Widget>[
                          PictureStyle(22,15,0),
                        Positioned(
                          left:25,
                          child:PictureStyle(22,15,1),
                        ),
                          Positioned(
                            left: 50,
                            child: PictureStyle(22,15,2),
                          ),
                          Positioned(
                            left:75,
                            child:PictureStyle(22,15,3),
                          )
                        ],

                      ),
                    )
                  ],
                ),
                PictureStyle(55,45,1),
              ],
            )

          ),
          SizedBox(
            height: 30.0,
          ),
          Container(


            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(25.0),
                topLeft: Radius.circular(25.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(-5, 5),
          ),],),


                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Pilates for Beginners',

                    style: TextStyle(fontSize: 24,fontWeight: FontWeight.w500,height: 1.21,),),
                      SizedBox(height:10.0),
                      Row(
                    children: <Widget>[
                           Rating(4),
                          SizedBox(
                         width:10.0,
                    ),
                            Text('('+reviewNo.toString()+' reviews)',style: TextStyle(
                        color: Color(0xFFE9707D),
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                    ),),


                      ],
                    ),
                      SizedBox(
                        height: 15.0,
                      ),
                      WorkoutDetails(6,12,12,60),
                      SizedBox(height:25.0,),
                      Text('Schedule',style:TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.w500,
                        color:Color(0xFF303030),
                      ),),
                      SizedBox(
                        height:10.0,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: exercisesList,



                      ),
                      SizedBox(
                        height:20.0,
                      ),
                      Center(
                        child: RaisedButton(

                          color: Color(0xFFFFFFFF),
                          textColor: Color(0xFFE9707D),
                          child: Text('UNJOIN', style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18.0,
                          ),),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0),),

                          onPressed: (){

                          },
                        ),
                      ),

                  ],
                ),

            ),

        ],),

        )
      );



  }
}

class TrainerName extends StatelessWidget {

  final String trainersName;
  TrainerName(this.trainersName);
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: 'with ',
            style: TextStyle(
              fontSize: 14.0,
              color: Color(0xFF303030),
              height: 1.21,

            ),),
      TextSpan(
        text:trainersName,
        style: TextStyle(
            fontSize: 14.0,
            color: Colors.red,
            height: 1.21,

          ),),
          TextSpan(
            text: ' and',
              style: TextStyle(
                fontSize: 14.0,
                color: Color(0xFF303030),
                height: 1.21,


              ),),
        ]
      ),
    );
  }
}



class WorkoutDate extends StatelessWidget {

  final String workoutDate;
  final String workoutHour;

  WorkoutDate(this.workoutDate,this.workoutHour);

  @override
  Widget build(BuildContext context) {
    return Text(workoutDate + '-' + 'At '+workoutHour,
    style: TextStyle(
      fontSize: 13.0,
      color: Color(0xFF393E46),
      height: 1.23,
    ),);
  }
}

class WorkoutName extends StatelessWidget {
  final String workoutName;

  WorkoutName(this.workoutName);

  @override
  Widget build(BuildContext context) {
    return Text(workoutName,style: TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.w500,
      color: Color(0xFF303030),
      height: 1.20,
      //fontFamily: 'Montserrat',
    ),);
  }
}
