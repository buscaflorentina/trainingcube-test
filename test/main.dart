import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'participants_card.dart';
import 'search_bar.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ParticipantsCard participant1=ParticipantsCard(0,'Ana Maria','body pump, c-core, circuit training, \ninterval');
    ParticipantsCard participant2=ParticipantsCard(1,'Christian Willis','metabolic body workout, pilates, c-core');
    ParticipantsCard participant3=ParticipantsCard(2,'Kate David','circuit training, interval, metabolic \nbody workout');
    ParticipantsCard participant4=ParticipantsCard(3,'Ioana Kate','body pump, c-core, circuit training, \npilates');
    InputDecoration search=InputDecoration(hintText: 'Search',prefixIcon: Icon(Icons.search));

    List<Widget> participantsList=[

      participant1,
      participant2,
      participant3,
      participant4
    ];

    return MaterialApp(
      home:SafeArea(
        child: Scaffold(
          backgroundColor: Colors.grey[200],
          persistentFooterButtons: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,

            children: <Widget>[
              Container(
                height: 70.0,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.ac_unit,
                    color: Colors.red,
                    size: 40.0
                    ),
                    Text('Events',
                    style: TextStyle(color: Colors.red,
                    fontSize: 16.0),
                    ),
                  ],
                ),
              ),

              SizedBox(
                width:40.0,
              ),


              Container(
                height: 70.0,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.people_outline,
                    color: Colors.grey[700],
                    size:40.0,
                    ),
                    Text('Follow',
                    style: TextStyle(
                      fontSize: 16.0,
                      color:Colors.grey[700],
                    ),)
                  ],
                ),
              ),
              SizedBox(
                width:40.0,
              ),

              Container(
                height: 70.0,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.graphic_eq,
                    color: Colors.grey[700],
                    size:40.0),
                    Text('Leaderboard',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey[700],
                    ),)
                  ],
                ),
              ),
              SizedBox(
                width:40.0,
              ),

              Container(
                height: 70.0,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.person_outline,
                    color: Colors.grey[700],
                    size:40.0),
                    Text('Profile',
                    style:TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey[700],
                    ),),
                  ],
                ),
              ),
              SizedBox(
                width:20.0,
              ),





            ],),],




          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.grey[100],
            elevation: 0.0,
            title: Text('Participants',
            style: TextStyle(
              color:Colors.grey[700],
            ),),

          ),
          body:
          Column(
           children: <Widget>[
             TextField(decoration: search,

             ),
             SizedBox(
               height:20.0,
             ),




             Column(
               mainAxisAlignment: MainAxisAlignment.center,
               children: participantsList,
             ),
           ],






      ),
        ),),);


  }}