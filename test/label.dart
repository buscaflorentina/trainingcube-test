import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Label extends StatelessWidget {
  String text1;
  String text2;
  String text3;

  Label(this.text1,this.text2,this.text3);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(


      padding: EdgeInsets.only(left:1.0, right: 1.0, top: 1.0, bottom: 1.0),
      decoration: BoxDecoration(
        color: Colors.purple,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(5.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 5,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Center(
        child:Icon(Icons.graphic_eq,color: Colors.white,),),
      ),
        SizedBox(
            width:10.0
        ),
        Text(text1),
        SizedBox(
          width:20.0
        ),
        Container(


          padding: EdgeInsets.only(left:1.0, right: 1.0, top: 1.0, bottom: 1.0),
          decoration: BoxDecoration(
            color: Colors.red,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(5.0),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.5),
                spreadRadius: 0,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Center(
            child:Icon(Icons.star,color:Colors.white),),
        ),
    SizedBox(
    width:10.0
    ),
    Text(text2),
    SizedBox(
    width:20.0),

        Container(


          padding: EdgeInsets.only(left:1.0, right: 1.0, top: 1.0, bottom: 1.0),
          decoration: BoxDecoration(
            color: Colors.orange,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(5.0),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.5),
                spreadRadius: 0,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Center(
            child:Icon(Icons.access_alarms,color:Colors.white),),
        ),
        SizedBox(
            width:10.0
        ),
        Text(text3),
        SizedBox(
            width:20.0),
      ],);



  }
}
