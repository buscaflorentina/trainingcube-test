import 'package:flutter/material.dart';

class CustomDecoration{
  static InputDecoration textFieldStyle({String hintTextStr, Icon prefixIcon}){
    return InputDecoration(
      contentPadding: EdgeInsets.all(15.0),
      prefixIcon: prefixIcon,
      hintText: hintTextStr,

      hintStyle: TextStyle(
        color: Colors.grey[400],
        fontFamily: 'Montserrat',
        fontSize: 15,
      ),
      floatingLabelBehavior: FloatingLabelBehavior.never,
      filled: true,
      fillColor: Colors.white,
      border: InputBorder.none,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Color.fromARGB(255, 220, 220, 220),
          width: 1.0,
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: Color.fromARGB(255, 200, 200, 200),
            width: 1.0),
      ),
    );
  }
}