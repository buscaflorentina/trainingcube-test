import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'label.dart';

class ParticipantsCard extends StatelessWidget{
 int cardNumber;
 String participantsName;
 String participantsActivities;


 ParticipantsCard(this.cardNumber,this.participantsName,this.participantsActivities);

 List<Label> labelList=[
   Label('6','10','40 h'),
   Label('9','5','5 h'),
   Label('6','12','20 h'),
   Label('1','25','30 h')
 ];
 @override
 Widget build(BuildContext context) {
 return  Card(
  shadowColor: Colors.grey[250],
  color: Colors.grey[100],
  elevation: 20.0,
  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(25.0),) ,
  child:Row(
    children: <Widget>[
       CircleAvatar(
          radius:43.0,
          backgroundColor: Colors.white,
           child: CircleAvatar(
              backgroundImage: AssetImage('images/girl$cardNumber.png'),
              radius:33.0,
    ),
    ),
      SizedBox(
        width: 40.0,
      ),
      Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

             Text(participantsName,
                  style: TextStyle(
                  fontSize: 20.0,
                    color: Colors.grey[700],
        ),),
              Text(participantsActivities,
                       style: TextStyle(
                       fontSize: 12.0,
                       fontStyle: FontStyle.italic,
                        color:Colors.grey[500],
  ),
              ),
            labelList[cardNumber],
          ],
        ),
    ],
  ),
 );


}}